from django.conf.urls import url
from . import views


post_list = views.PostViewSet.as_view({
    'get': 'list',
    'post': 'create',
})

post_detail = views.PostViewSet.as_view({
    'get': 'retrieve',
    'patch': 'partial_update',
    'delete': 'destroy',
})

app_name = 'api'

urlpatterns = [
    url('posts/$', post_list, name='post-list'),
    url('posts/(?P<pk>.+)/$',  post_detail, name='post-detail'),
]
