from rest_framework import serializers
from applications.blogs.models import Post


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = (
            'id',
            # 'user',
            'title',
            'content',
            'publishing_date',
            # 'image',
            'slug',
        )