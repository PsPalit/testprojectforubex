from rest_framework import mixins, viewsets, permissions, decorators
from django_filters.rest_framework import DjangoFilterBackend
from applications.blogs.models import Post
from .serializers import PostSerializer
from .filters import PostFilter


class PostViewSet(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  viewsets.GenericViewSet):

    serializers = {
        'list': PostSerializer,
        'create': PostSerializer,
        'retrieve': PostSerializer,
        'partial_update': PostSerializer,
        'destroy': PostSerializer,
        'metadata': PostSerializer,
    }

    filter_backends = (DjangoFilterBackend,)
    filter_class = PostFilter

    lookup_field = 'pk'

    def get_serializer_class(self):
        return self.serializers.get(self.action)

    def get_queryset(self):
        return Post.objects.all()
