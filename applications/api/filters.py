import django_filters as filters
from applications.blogs.models import Post


class PostFilter(filters.FilterSet):

    order_by_field = 'ordering'

    order_by = filters.OrderingFilter(
        # fields(('model field name', 'parameter name'),)
        fields=(
            ('pk', 'id'),
            ('title', 'title'),
            ('publishing_date', 'publishing_date'),
        )
    )

    class Meta:
        model = Post
        fields = ['id', 'title', 'content', 'publishing_date', 'slug']
