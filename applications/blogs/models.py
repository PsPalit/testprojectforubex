from django.db import models
from django.urls import reverse
from uuslug import slugify
# from ckeditor.fields import RichTextField


class Post(models.Model):
    # user = models.ForeignKey('auth.User', related_name='posts', on_delete=models.CASCADE)
    title = models.CharField(max_length=120, verbose_name='Название')
    content = models.TextField(verbose_name='Содержание')
    publishing_date = models.DateTimeField(verbose_name='Дата публикации', auto_now_add=True)
    # image = models.ImageField(null=True, blank=True, verbose_name='Изображение')
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.title

    def get_unique_slug(self):
        slug = slugify(self.title)
        unique_slug = slug
        counter = 1
        while Post.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, counter)
            counter += 1
        return unique_slug

    def save(self, *args, **kwargs):
        self.slug = self.get_unique_slug()
        return super(Post, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-publishing_date', 'id']


# class Comment(models.Model):
#     post = models.ForeignKey('post.Post', on_delete=models.CASCADE, related_name='comments')
#     name = models.CharField(max_length=200, verbose_name='Тема')
#     content = models.TextField(verbose_name='Комментарий')
#
#     created_date = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return self.name
#
#     class Meta:
#         ordering = ['-created_date', 'id']
