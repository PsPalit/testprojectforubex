from django.test import TestCase
from django.contrib.auth.models import User

from applications.blogs.models import Post


class PostModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        user = User.objects.create_user(username='test_ubex', password='12345')
        Post.objects.create(user=user, title='Test Ubex', content='This project is Ubex')
        Post.objects.create(user=user, title='Test Ubex', content='This project is Ubex')

    def test_slug_field_is_unique(self):
        post_one = Post.objects.get(id=1)
        post_two = Post.objects.get(id=2)
        self.assertEquals(post_one.slug, post_two.slug)

